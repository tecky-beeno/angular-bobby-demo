import { Component, OnInit } from '@angular/core'

type Hole = {
  active: boolean
}

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  rows: Hole[][]
  score = 0

  constructor() {
    this.rows = []
    for (let row = 0; row < 3; row++) {
      let holes: Hole[] = []
      this.rows[row] = holes
      for (let col = 0; col < 3; col++) {
        holes.push({ active: false })
      }
    }
  }

  ngOnInit(): void {
    setInterval(() => this.randomAppear(), 1000)
  }

  randomAppear() {
    let row = Math.floor(Math.random() * 3)
    let col = Math.floor(Math.random() * 3)
    this.rows[row][col].active = true
  }

  checkHole(hole: Hole) {
    if (hole.active) {
      hole.active = false
      this.score++
    }
  }
}
