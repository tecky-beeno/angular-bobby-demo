import { Component } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  todoList: string[]

  text = ''

  constructor() {
    let text = localStorage.getItem('todoList') || '[]'
    this.todoList = JSON.parse(text)
  }

  complete(item: string) {
    this.todoList = this.todoList.filter(x => x != item)
  }

  addItem() {
    if (!this.text) {
      return
    }
    this.todoList.push(this.text)
    let text = JSON.stringify(this.todoList)
    localStorage.setItem('todoList', text)
    this.text = ''
  }
}
